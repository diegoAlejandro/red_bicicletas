var map = L.map('main_map').setView([4.8048592, -75.7487813], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(map);

var circle = L.circle([4.8048592, -75.7487813], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map);

L.marker([4.8048592, -75.7487813]).addTo(map)
    .bindPopup('INGSERCOM.')
    .openPopup();
